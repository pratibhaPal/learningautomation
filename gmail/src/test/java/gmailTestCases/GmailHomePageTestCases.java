package gmailTestCases;

import org.testng.Assert;
import org.testng.annotations.Test;
import constant.Constant;
import testHook.GetDriver;

public class GmailHomePageTestCases {
	@Test
	public void testmMarkStarEmail(){
		String msg = GetDriver
				.getGmailLoginpage()
				.gmailLogIn()
				.markStarEmail()
				.getText();
		Assert.assertTrue(msg.contains(Constant.EMAIL_SUBJECT), "Not verified !!!");
	}
	
	@Test
	public void testEmailInDraft(){
		String msg = GetDriver
				.getGmailLoginpage()
				.gmailLogIn()
				.cancelEmail()
				.getText();
		Assert.assertTrue(msg.contains(Constant.EMAIL_TEXT), "Not verified !!!");
	}
	
	@Test
	public void testsearchEmail(){
		String msg = GetDriver
				.getGmailLoginpage()
				.gmailLogIn()
				.searchEmail()
				.getText();
		Assert.assertTrue(msg.contains(Constant.SEARCH), "Not verified !!!");
	}
}
