package pageObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import testHook.GetDriver;
import util.WaitMethods;
import constant.Constant;

public class GmailLoginPage extends BasePage{
	
	public GmailLoginPage(WebDriver driver){
		super(driver);
		PageFactory.initElements(driver, this);
		WaitMethods.waitForPageLoaded(driver, 30);
	}
	
	@FindBy(id="identifierId") 
	WebElement emailId; 
	
	@FindBy(id="identifierNext")
	WebElement nextButton;
	
	@FindBy(name="password")
	WebElement password;
	
	@FindBy(id="passwordNext")
	WebElement passwordNextButton;
	
	public GmailHomePage gmailLogIn() {
		emailId.sendKeys(Constant.EMAIL);
		nextButton.click();
		password.sendKeys(Constant.PASSWORD);
		passwordNextButton.click();
		return new GmailHomePage(driver);
	}
	
}
