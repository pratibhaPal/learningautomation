package pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import java.util.List;
import constant.Constant;
import util.WaitMethods;

public class GmailHomePage extends BasePage{
	
	public GmailHomePage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
		WaitMethods.waitForPageLoaded(driver, Constant.TIME_OUT_SECONDS);
	}
	
	@FindBy(xpath="//*[@class='apU xY']")
	List<WebElement> emails;
	
	@FindBy(linkText="Starred")
	WebElement starred;
	
	@FindBy(xpath="//*[@class='T-I J-J5-Ji T-I-KE L3']")
	WebElement composeButton;
	
	@FindBy(className="vO")
	WebElement toTextField;
	
	@FindBy(className="aoT")
	WebElement subjectTextField;
	
	@FindBy(xpath="//img[@class='Ha']")
	WebElement cancelButton;
	
	@FindBy(linkText="Drafts")
	WebElement drafts;
	
	@FindBy(xpath="//input[@name='q']")
	WebElement search;
	
	private By starredButton = By.linkText("Starred");
	
	public GmailCommonPage markStarEmail() {
		
		WaitMethods.waitForVisibility(driver, starredButton);
		for (int i=1; i<= emails.size(); i++)
		{
			if(i == 1) {
				emails.get(i-1).click();
			}
		}
		driver.navigate().refresh();
		starred.click();
		return new GmailCommonPage(driver);
	}
	
	public GmailCommonPage cancelEmail() {
		WaitMethods.waitForVisibility(driver, starredButton);
		composeButton.click();
		toTextField.sendKeys(Constant.TO_EMAIL);
		subjectTextField.sendKeys(Constant.EMAIL_SUBJECT);
		cancelButton.click();
		drafts.click();
		return new GmailCommonPage(driver);
	}
	
	public GmailCommonPage searchEmail() {
		WaitMethods.waitForPageLoaded(driver, Constant.TIME_OUT_SECONDS);
		search.sendKeys(Constant.SEARCH + "\n");
		return new GmailCommonPage(driver);
	}
}
