package pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import constant.Constant;
import util.WaitMethods;

public class GmailCommonPage extends BasePage{
	public GmailCommonPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
		WaitMethods.waitForPageLoaded(driver, Constant.TIME_OUT_SECONDS);
	}
	
	public String getText() {
		WaitMethods.waitForPageLoaded(driver, Constant.TIME_OUT_SECONDS);
		String test = driver.findElement(By.tagName("body")).getText();
		System.out.println(test);
		return test;
	}
}
