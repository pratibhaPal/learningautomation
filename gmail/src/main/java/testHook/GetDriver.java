package testHook;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import constant.Constant;
import pageObject.GmailLoginPage;

public class GetDriver {
	private static WebDriver driver;

    public static WebDriver getDriver() {
        if (driver != null) {
            return driver;
        }

        System.setProperty("webdriver.chrome.driver", Constant.DRIVER_PATH);
		driver = new ChromeDriver();
        
		driver.manage().window().maximize();
		driver.get(Constant.GMAIL_BASE_URL);
		
		driver.manage().timeouts().implicitlyWait(Constant.TIME_OUT_SECONDS, TimeUnit.SECONDS);
        return driver;
    }

    public static GmailLoginPage getGmailLoginpage() {
    	
		return new GmailLoginPage(getDriver());
    }
}

