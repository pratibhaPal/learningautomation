package mavenProject.maven;

public class Constant {
	public static final String BASE_URL = "https://reqres.in/api";
	public static final int VALID_USER_ID = 2;
	public static final int INAUTHENTIC_USER_ID = 1000;
	public static final String INVALID_USER_ID = "dfgdf";
	public static final String FIRST_NAME = "first_name";
	public static final String JOB = "job";
}
