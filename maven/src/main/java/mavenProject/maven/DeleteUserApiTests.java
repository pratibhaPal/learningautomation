package mavenProject.maven;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import io.restassured.RestAssured;
import static io.restassured.RestAssured.given;
import mavenProject.maven.Constant;

public class DeleteUserApiTests{
	@BeforeClass
	public void setBaseUri () {
		RestAssured.baseURI = Constant.BASE_URL;
	}
	@Test
	public void deleteUser(){
		given()
		.when()
		.delete("users/" + Constant.VALID_USER_ID)
		.then().statusCode(204);
	}
	@Test
	public void deleteInvalidUser(){
		given()
		.when()
		.delete("users/" + Constant.INVALID_USER_ID)
		.then().statusCode(204);
	}
}
