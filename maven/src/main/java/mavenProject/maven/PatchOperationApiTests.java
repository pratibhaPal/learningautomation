package mavenProject.maven;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.response.ValidatableResponse;
import org.json.simple.JSONObject;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import mavenProject.maven.Constant;
import java.util.logging.Logger;

public class PatchOperationApiTests {
	private static Logger Log = Logger.getLogger(PatchOperationApiTests.class.getName());
	@BeforeClass
	public void setBaseUri () {
		RestAssured.baseURI = Constant.BASE_URL;
	}
	@Test
	public void updateParticularFieldTestCase(){	
		JSONObject persons = new JSONObject();
		persons.put(Constant.FIRST_NAME, "pal");
		JSONObject data = new JSONObject();
		data.put("data", persons);
		ValidatableResponse response = given()
		.contentType("application/json")
		.body(data)
		.when()
		.patch("/users/" + Constant.VALID_USER_ID)
		.then()
		.statusCode(200).body("data.first_name", equalTo("pal"));
		Log.info(response.extract().asString());
	}
	@Test
	public void InvalidupdateParticularFieldTestCase(){	
		JSONObject persons = new JSONObject();
		persons.put(Constant.FIRST_NAME, "");
		JSONObject data = new JSONObject();
		data.put("data", persons);
		ValidatableResponse response = given()
		.contentType("application/json")
		.body(data)
		.when()
		.patch("/users/" + Constant.VALID_USER_ID)
		.then()
		.statusCode(200).body("data.first_name", equalTo(""));
		Log.info(response.extract().asString());
	}
}
