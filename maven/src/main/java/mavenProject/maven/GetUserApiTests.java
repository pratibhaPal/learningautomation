package mavenProject.maven;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.response.ValidatableResponse;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.greaterThan;
import mavenProject.maven.Constant;
import java.util.logging.Logger;

public class GetUserApiTests{
	private static Logger Log = Logger.getLogger(GetUserApiTests.class.getName());
	@BeforeClass
	public void setBaseUri () {
		RestAssured.baseURI = Constant.BASE_URL;
	}
	@Test
	public void login(){   
		ValidatableResponse res  = given ()
			    .when()
			    .get ("/users/" + Constant.VALID_USER_ID)
			    .then().assertThat().statusCode(200).body("data.size()", greaterThan(0));
		Log.info(res.extract().asString());
	}
	@Test
	public void inauthenticUser(){   
		ValidatableResponse res  = given ()
			    .when()
			    .get ("/users/" + Constant.INAUTHENTIC_USER_ID)
			    .then().assertThat().statusCode(404);
		Log.info(res.extract().asString());
	}
	@Test
	public void invalidUser(){   
		ValidatableResponse res  = given ()
			    .when()
			    .get ("/users/" + Constant.INVALID_USER_ID)
			    .then().assertThat().statusCode(404);
		Log.info(res.extract().asString());
	}
}
