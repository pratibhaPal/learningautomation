package testHook;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import constant.Constant;
import pageObject.LoginPage;

public class GetDriver {
	private static WebDriver driver;

    public static WebDriver getDriver() {
        if (driver != null) {
            return driver;
        }
//        System.setProperty("webdriver.ie.driver", Constant.IE_DRIVER);
//        WebDriver driver = new InternetExplorerDriver();
//       
        System.setProperty("webdriver.chrome.driver", Constant.CHROME_DRIVER);
		driver = new ChromeDriver();
//        System.setProperty("webdriver.gecko.driver", Constant.FIREFOX_DRIVER);
//		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get(Constant.FLIPKART_URL);
		// driver.get(Constant.BOOKBOON_URL);
		driver.manage().timeouts().implicitlyWait(Constant.TIMEOUT_TIME, TimeUnit.SECONDS);
        return driver;
    }

    public static LoginPage getLoginpage() {
    	
		return new LoginPage(getDriver());
    }
}

