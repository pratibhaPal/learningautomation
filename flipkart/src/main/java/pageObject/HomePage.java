package pageObject;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.logging.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import constant.Constant;
import util.WaitMethods;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import util.WaitMethods;
public class HomePage extends BasePage{
	
	private static Logger Log = Logger.getLogger(HomePage.class.getName());
	
	public HomePage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
		WaitMethods.waitForPageLoaded(driver, 30);
	}
	
	@FindBy(xpath = "//img[@src='https://rukminim1.flixcart.com/www/200/200/promos/10/10/2018/96e4223d-64fd-446d-b548-d8563de1aea6.png?q=90']") 
	private WebElement mobiles;
	
	@FindBy(xpath = "//img[@src='https://rukminim1.flixcart.com/flap/50/50/image/303b32d8065cba1d.jpg?q=50']") 
	private WebElement redmi6Img;
	
	@FindBy(xpath = "//*[@class='_2oLiqr']")
	private WebElement wishListButton;
	
	@FindBy(xpath = "//*[@class= '_2cyQi_']")
	private WebElement myAccountButton;
	
	@FindBy(xpath = "//a[@href = '/wishlist?link=home_wishlist']")
	private WebElement wishListLink;
	
	@FindBy(xpath="//input[@name='q']")
	private WebElement searchTextField;
	
	@FindBy(xpath="//button[@type = 'submit']")
	private WebElement searchButton;
	
	@FindBy(xpath="//div[@class='_1HmYoV _35HD7C col-10-12']")
	private List<WebElement> allProduts;
	
	@FindBy(xpath="//div[@class='_2bVfmg']")
	private WebElement cartButton;
	
	@FindBy(xpath="//div[@class='_3ycxrs']")
	private List<WebElement> addedItems;
	
	@FindBy(xpath="//div[@class='_1qKb_B']//select")
	private WebElement minPriceRange;
	
	@FindBy(xpath="//div[@class='_1YoBfV']//select")
	private WebElement maxPriceRange;

	public HomePage addProduct() {
		mobiles.click();
		WaitMethods.waitForPageLoaded(driver, 30);
		redmi6Img.click();
		WaitMethods.waitForPageLoaded(driver, 30);
		wishListButton.click();
		Actions actions1 = new Actions(driver);
        actions1.moveToElement(myAccountButton).build().perform();
        wishListLink.click();
        try{
            driver.findElement(By.xpath("//div[contains(.,'Realme 2 (Diamond Black, 64 GB)')]"));
        }catch(NoSuchElementException e){
            Assert.fail();
        }
        return this;
	}
	
	public HomePage searchIphone6(){
		
		WaitMethods.waitForVisibility(driver, By.xpath("//div[@class='_2bVfmg']"));
		searchTextField.sendKeys("iphone 6");
		searchButton.click();
		Select minPriceRangeSelect = new Select(minPriceRange);
		minPriceRangeSelect.selectByVisibleText("₹20000");
		
		Select maxPriceRangeSelect = new Select(maxPriceRange);
		maxPriceRangeSelect.selectByVisibleText("₹30000");
		
		WaitMethods.waitForVisibility(driver,By.xpath("//div[@class='_2bVfmg']"));
		List <WebElement> listofItems = allProduts;
		WebDriverWait wait = new WebDriverWait(driver, Constant.TIMEOUT_TIME); 
		for (int i=1; i<=listofItems.size(); i++)
		{ 
		    listofItems = driver.findElements(By.xpath("//a[@class='_31qSD5']"));
		    wait.until(ExpectedConditions.visibilityOf(listofItems.get(i-1)));
		    String parentHandle = driver.getWindowHandle(); // get the current window handle
		    listofItems.get(i-1).click();   
		    for (String winHandle : driver.getWindowHandles()) { 
		    	driver.switchTo().window(winHandle);
		    }
		    if(driver.findElements( By.xpath("//button[@class='_2AkmmA _2Npkh4 _2MWPVK']") ).size() != 0) {
		    	WebElement addToCart = driver.findElement(By.xpath("//button[@class='_2AkmmA _2Npkh4 _2MWPVK']"));
		    	addToCart.click();
		    }
		    driver.close();                                 
		    driver.switchTo().window(parentHandle);   
		} 
		cartButton.click();
		Log.info("Total products = " + addedItems.size());
		String totalAmount = driver.findElement(By.xpath("//div[@class='hJYgKM']/span")).getText();
		Log.info("Total amount = " + totalAmount);
		return this;
	}
	  
}