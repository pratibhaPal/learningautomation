package pageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import util.WaitMethods;
import constant.Constant;

public class LoginPage extends BasePage {
	
	public LoginPage(WebDriver driver){
		super(driver);
		PageFactory.initElements(driver, this);
		WaitMethods.waitForPageLoaded(driver, 30);
	}
	
	@FindBy(xpath="//input[@class='_2zrpKA']")
	private WebElement userNameTextField;
	
	@FindBy(xpath="//input[@type='password']")
	private WebElement passwordTextField;

	@FindBy(xpath="//Button[@type='submit']/span")
	private WebElement signInButton;
   
	public HomePage flipkartLogIn() {
		userNameTextField.sendKeys(Constant.USERNAME);
		passwordTextField.sendKeys(Constant.PASSWORD);
		signInButton.click();
		return new HomePage(driver);
	}
	
}
