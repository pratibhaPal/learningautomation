package pageObject;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import constant.Constant;

public class AddAndDownload extends BasePage{

	public AddAndDownload(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = "//a[@href='i-still-cant-speak-english-ebook']") 
	private WebElement bookLink;
	
	@FindBy(xpath = "//input[@placeholder='Studying or working?']") 
	private WebElement firstQuestions;
	
	@FindBy(xpath = "//input[@placeholder='Study program?']") 
	private WebElement secondQuestions;
	
	@FindBy(xpath = "//input[@placeholder='University?']") 
	private WebElement ThirdQuestions;
	
	@FindBy(xpath = "//input[@value='true']") 
	private WebElement acceptCheckbox;
	
	@FindBy(id = "email") 
	private WebElement email;
	
	@FindBy(xpath = "//button[@type='submit']") 
	private WebElement submitButton;
	
	JavascriptExecutor je = (JavascriptExecutor) driver;
	
	public void downloadAndSave() {
		
		firstQuestions.sendKeys("Studying" , Keys.TAB);
		
		WebDriverWait wait = new WebDriverWait(driver, Constant.TIMEOUT_TIME);
		wait.until(ExpectedConditions.visibilityOf(secondQuestions));
		
		secondQuestions.sendKeys("Information Tech MSc", Keys.TAB);
		
		wait.until(ExpectedConditions.visibilityOf(ThirdQuestions));
		
		ThirdQuestions.sendKeys("Anna University, Chennai", Keys.TAB);
		
		wait.until(ExpectedConditions.visibilityOf(acceptCheckbox));
		
		je.executeScript("arguments[0].scrollIntoView(true);",acceptCheckbox);
		acceptCheckbox.click();
		
		je.executeScript("arguments[0].scrollIntoView(true);",email);
		email.sendKeys("pratibha.pal@intimec.com");
		
		je.executeScript("arguments[0].scrollIntoView(true);",submitButton);
		submitButton.click();
	}
}
