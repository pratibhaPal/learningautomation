package util;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WaitMethods {

	public static void waitForPageLoaded(WebDriver webdriver, int time) {

		ExpectedCondition<Boolean> expectation = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				
				return ((JavascriptExecutor) driver).executeScript(
						"return document.readyState").equals("complete");
			}
		};
		
		Wait<WebDriver> wait = new WebDriverWait(webdriver, time);
		try {
			wait.until(expectation);
		} catch (Throwable error) {

		}
	}
	public static WebDriverWait getWebDriverWait(WebDriver driver, long timeOutInSeconds, long sleepInMillis) {
        return new WebDriverWait(driver, timeOutInSeconds, sleepInMillis);
    } 
	public static void waitForVisibility(WebDriver webdriver,  By element){
        getWebDriverWait(webdriver, 180, 20).until(ExpectedConditions.presenceOfAllElementsLocatedBy(element));
    } 
}
