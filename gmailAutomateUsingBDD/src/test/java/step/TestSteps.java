package step;
import static org.junit.Assert.assertTrue;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import util.WaitMethods;

public class TestSteps {
	
	public static WebDriver driver;
	
	@Given("^I am on home page$")
	public void i_am_on_home_page() throws Throwable {
		System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://www.gmail.com");
	}
	
	@When("^I Enter Email \"(.*?)\" and password \"(.*?)\"$")
	public void i_Enter_Email_and_password(String arg1, String arg2) throws Throwable {
		driver.findElement(By.id("identifierId")).sendKeys(arg1);
	    driver.findElement(By.id("identifierNext")).click();
	    WaitMethods.waitForVisibility(driver, By.xpath("//input[@name='password']"));
	    driver.findElement(By.xpath("//input[@name='password']")).sendKeys(arg2);
	    driver.findElement(By.id("passwordNext")).click();
	}
	
	@Then("^Home page should be displayed$")
	public void home_page_should_be_displayed() throws Throwable {
		WaitMethods.waitForVisibility(driver, By.xpath("//*[@class='apU xY']"));
		assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Compose[\\s\\S]*$"));
	}
	
	@When("^I Click on compose button and I Enter emailId ,subject and body and click Send button$")
	public void i_Click_on_compose_button_and_I_Enter_emailId_subject_and_body_and_click_Send_button() throws Throwable {
		driver.findElement(By.xpath("//*[@class=\"T-I J-J5-Ji T-I-KE L3\"]")).click();
	    driver.findElement(By.className("vO")).sendKeys("pratibha.pal@intimetec.com");
	    driver.findElement(By.className("aoT")).sendKeys("This is an auto-generated mail");
	    driver.findElement(By.xpath("//div[@class='Ar Au']//div")).sendKeys("Hi Avinash");
	    driver.findElement(By.xpath("//div[text()='Send']")).click();
	    
	}
	
	@When("^I click Sent Button$")
	public void i_click_Sent_Button() throws Throwable {
		WaitMethods.waitForPageLoaded(driver, 50);
		driver.findElement(By.linkText("Sent")).click();
	}
	
	@Then("^Sent Emails Should display\\.$")
	public void sent_Emails_Should_display() throws Throwable {
		WaitMethods.waitForPageLoaded(driver, 50);
		assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Hi Avinash[\\s\\S]*$"));
	}
}
